<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Technologies used

Back end:
- Laravel.

Front end:
- Javascript: Vue/VueX.
- Css: Tailwind.

## Short description

The application consumes data from the twitch IGDB api, about game reviews. We then process that data in a way that is most suitable for our application and send it to the front end.

## Architecture

We have a modular architecture ( Front end, Reviews, Security ), each module with its own responsibilities.


Front end module:
- This is the simplest module of them all, used only for returning views and defining the main routes of our application.

Security module:
- We use this module to check if our application can access the data from IGDB API. Here we have the default laravel security middlewares as well as logic for our oAuth tokens ( at the moment we only use the IGDB token ).

Reviews module:
- Our main application logic is stored in this module. We use this to make requests to the IGDB API, return the needed data, format it in a way that is most suitable for the front end and ultimately return it.


## Basic application flow

- Front end module return a view.
- Front end (js) makes a request to the Reviews module.
- Security module validates the oAuth token from IGDB API.
- Reviews module makes a request to IGDB API and formats the data in the way the front end expects it.
- Front end receives the needed data.

## Principles and standards

The code is mainly focused on SOLID principles, together with PSR standards.

## Versions

- Php 7.2.34
- Laravel 7.30.1
- Composer 1.10.8
- Npm 6.14.12
- Node 12.22.1

## Project set up

- First you will need to create a Twitch Account. Detailed instructions [here](https://api-docs.igdb.com/#account-creation).
- Copy the .env.example into a .env file.
- After you get your client-id and client-secret go ahead and add them to the config file.
- Set up your database settings in the config file.
- Run the usual “composer install”, after that “npm install” and finally “npm run dev” - to get the project up and going.
- Run php artisan key:generate.
- Run php artisan migrate ( this will create database tables ).
- Run php artisan db:seed ( this will create a mock IGDB token that has expired, so that the Security module can then refresh it ).
- We can avoid creating a virtual host by a magic command that laravel has: php artisan serve.
- Head on to the IP displayed in the console and the application should run with no problems.


