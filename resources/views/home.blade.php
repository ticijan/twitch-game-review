@extends('layout.layout')

@section('content')
    <div id="home" class="bg-blue-100">
        <home></home>
    </div>
@endsection

@section('scripts')
    <script src="{{ mix('js/home.js') }}"></script>
@endsection
