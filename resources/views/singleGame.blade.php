@extends('layout.layout')

@section('content')
    <div id="singleGame" class="bg-blue-100">
        <single_game></single_game>
    </div>
@endsection

@section('scripts')
    <script>
        var slug = '{{ $slug }}';
    </script>
    <script src="{{ mix('js/singleGame.js') }}"></script>
@endsection
