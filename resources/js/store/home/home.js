const state = {
    popular: [],
    comingSoon: [],
    recentlyReviewed: []
}

const getters = {
    getPopularGames: state => {
        return state.popular;
    },
    getComingSoonGames: state => {
        return state.comingSoon;
    },
    getRecentlyReviewedGames: state => {
        return state.recentlyReviewed;
    }
}

const mutations = {
    'SET_POPULAR_GAMES': (state, payload) => {
        state.popular = payload;
    },
    'SET_COMING_SOON_GAMES': (state, payload) => {
        state.comingSoon = payload;
    },
    'SET_RECENTLY_REVIEWED_GAMES': (state, payload) => {
        state.recentlyReviewed = payload;
    }
}

const actions = {
    setPopularGames({ commit }, payload) {
        commit('SET_POPULAR_GAMES', payload);
    },
    setComingSoonGames({ commit }, payload) {
        commit('SET_COMING_SOON_GAMES', payload);
    },
    setRecentlyReviewedGames({ commit }, payload) {
        commit('SET_RECENTLY_REVIEWED_GAMES', payload);
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
