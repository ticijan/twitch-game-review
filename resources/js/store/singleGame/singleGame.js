const state = {
    game: {
        cover: {},
        cover_image_url: '',
        critic_rating: '',
        first_release_date: null,
        genres: '',
        id: null,
        involved_companies: null,
        member_rating: '',
        name: '',
        platforms: '',
        screenshots: [],
        similar_games: [],
        slug: '',
        summary: '',
        trailer: null
    }
}

const getters = {
    getGame: state => {
        return state.game;
    },

    getCoverImage: state => {
        return state.game.cover_image_url;
    },

    getCriticRating: state => {
        return state.game.critic_rating;
    },

    getFirstReleaseDate: state => {
        return state.game.first_release_date;
    },

    getGenres: state => {
        return state.game.genres;
    },

    getId: state => {
        return state.game.id;
    },

    getInvolvedCompanies: state => {
        return state.game.involved_companies;
    },

    getMemberRating: state => {
        return state.game.member_rating;
    },

    getName: state => {
        return state.game.name;
    },

    getPlatforms: state => {
        return state.game.platforms;
    },

    getScreenshots: state => {
        return state.game.screenshots;
    },

    getSimilarGames: state => {
        return state.game.similar_games;
    },

    getSummary: state => {
        return state.game.summary;
    },

    getTrailer: state => {
        return state.game.trailer;
    },

    getSlug: state => {
        return state.game.slug;
    }
}

const mutations = {
    'SET_GAME': (state, payload) => {
        state.game = payload;
    }
}

const actions = {
    setGame({ commit }, payload) {
        commit('SET_GAME', payload);
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
