import Vue from 'vue';
import Vuex from 'vuex';

import singleGame from "./singleGame";

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        singleGame
    }
});
