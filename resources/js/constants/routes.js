var routes;

export default routes = Object.freeze({
    POPULAR_GAMES_GET: '/reviews/games/popular',
    COMING_SOON_GAMES_GET: '/reviews/games/soon',
    RECENTLY_REVIEWED_GAMES_GET: '/reviews/games/recent',

    SINGLE_GAME_GET: '/reviews/game/{slug}',

    SEARCH_GAMES_GET: '/reviews/search/{searchString}',

    SINGLE_GAME: '/game/{slug}'
});
