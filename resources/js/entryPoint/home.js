import Vue from 'vue'
import Home from '../vueComponents/home/Home.vue';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faBars, faGamepad } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { store } from "../store/home/store";

library.add(faBars);
library.add(faGamepad);
Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;

const home = new Vue({
    el: '#home',
    delimiters: ['${','}'],
    store,
    components: {
        home: Home
    }
});
