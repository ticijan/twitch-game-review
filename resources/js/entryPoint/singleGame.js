import Vue from 'vue'
import SingleGame from "../vueComponents/singleGame/SingleGame";

import { library } from '@fortawesome/fontawesome-svg-core';
import { faBars, faGamepad } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { store } from "../store/singleGame/store";

library.add(faBars);
library.add(faGamepad);
Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;

const single_game = new Vue({
    el: '#singleGame',
    delimiters: ['${','}'],
    store,
    components: {
        single_game: SingleGame
    }
});
