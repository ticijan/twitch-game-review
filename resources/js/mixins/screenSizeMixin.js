export const ScreenSizeMixin = {
    data() {
        return {
            screenSize: screen.width
        }
    },

    methods: {
        screenResize() {
            this.screenSize = screen.width;
        }
    },

    created() {
        window.addEventListener("resize", this.screenResize);
    },
    destroyed() {
        window.removeEventListener("resize", this.screenResize);
    },
}
