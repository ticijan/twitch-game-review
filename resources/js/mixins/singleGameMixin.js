import axios from "axios";
import routes from "../constants/routes";
import { mapActions } from "vuex";

export const SingleGameMixin = {
    methods: {
        ...mapActions('singleGame', [
            'setGame'
        ]),

        loadGame() {
            let that = this;
            let getGameUrl = routes.SINGLE_GAME_GET.replace('{slug}', window.slug);

            axios.get(getGameUrl).then(response => {
                if (response.data.success) {
                    that.setGame(response.data.games);
                }
            })
        }
    },
}
