import axios from "axios";
import routes from "../constants/routes";
import { mapActions } from "vuex";

export const ComingSoonGames = {
    methods: {
        ...mapActions('home', [
            'setComingSoonGames'
        ]),

        loadComingSoonGames() {
            let that = this;

            axios.get(routes.COMING_SOON_GAMES_GET).then(response => {
                if (response.data.success) {
                    that.setComingSoonGames(response.data.games);
                }
            })
        }
    },
}
