import axios from "axios";
import routes from "../constants/routes";
import { mapActions } from "vuex";

export const PopularGamesMixin = {
    methods: {
        ...mapActions('home', [
            'setPopularGames'
        ]),

        loadPopularGames() {
            let that = this;

            axios.get(routes.POPULAR_GAMES_GET).then(response => {
                if (response.data.success) {
                    that.setPopularGames(response.data.games);
                }
            })
        }
    },
}
