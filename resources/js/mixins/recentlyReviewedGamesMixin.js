import axios from "axios";
import routes from "../constants/routes";
import { mapActions } from "vuex";

export const RecentlyReviewedGames = {
    methods: {
        ...mapActions('home', [
            'setRecentlyReviewedGames'
        ]),

        loadRecentlyReviewedGames() {
            let that = this;

            axios.get(routes.RECENTLY_REVIEWED_GAMES_GET).then(response => {
                if (response.data.success) {
                    that.setRecentlyReviewedGames(response.data.games);
                }
            })
        }
    },
}
