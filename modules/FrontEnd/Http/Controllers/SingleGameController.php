<?php

namespace Modules\FrontEnd\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class SingleGameController extends Controller
{
    /**
     * @param string $slug
     * @return Application|Factory|View
     */
    public function index(string $slug)
    {
        return view('singleGame', compact('slug'));
    }
}
