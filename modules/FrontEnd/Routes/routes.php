<?php

use Illuminate\Support\Facades\Route;

// All route files should have a prefix ( preferred prefix would be the module name ).
// We exclude the FrontEnd module from this rule as it contains the main routes to our application.
Route::get('/', 'HomeController@index')->name('home.index');

Route::get('/game/{slug}', 'SingleGameController@index')->name('singleGame.index');
