<?php

namespace Modules\Security\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Modules\Security\Models\IgdbToken;

class OauthTokenService implements OauthTokenServiceInterface
{
    /** @inheritDoc */
    public function refreshIgdbToken(IgdbToken $token)
    {
        $refreshUrl = $this->getIgdbRefresTokenUrl();

        $response = Http::post($refreshUrl)->json();

        $token->update([
            'access_token' => $response['access_token'],
            'expires_in' => $response['expires_in']
        ]);

        return $token;
    }

    /** @return string */
    private function getIgdbRefresTokenUrl()
    {
        return Str::replaceArray(
            '{?}', [
                config('services.igdb.client_id'),
                config('services.igdb.client_secret')
            ],
            config('services.igdb.access_token_url')
        );
    }
}
