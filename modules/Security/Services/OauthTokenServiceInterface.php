<?php

namespace Modules\Security\Services;

use Modules\Security\Models\IgdbToken;

interface OauthTokenServiceInterface
{
    /**
     * @param IgdbToken $token
     * @return bool
     */
    public function refreshIgdbToken(IgdbToken $token);
}
