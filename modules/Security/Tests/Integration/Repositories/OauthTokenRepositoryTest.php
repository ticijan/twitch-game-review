<?php

namespace Modules\Security\Tests\Integration\Repositories;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\Security\Models\IgdbToken;
use Modules\Security\Models\InternalToken;
use Modules\Security\Repositories\OauthTokenRepository;
use Tests\TestCase;

class OauthTokenRepositoryTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @var OauthTokenRepository  */
    private $oAuthTokenRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->oAuthTokenRepository = new OauthTokenRepository();
    }

    public function testGetIgdbToken()
    {
        $createdIgdbTokenInstance = factory(IgdbToken::class)->create();

        $result = $this->oAuthTokenRepository->getIgdbToken();

        $this->assertSame($createdIgdbTokenInstance->id, $result->id);
    }
}
