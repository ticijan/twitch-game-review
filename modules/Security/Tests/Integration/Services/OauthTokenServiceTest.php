<?php

namespace Modules\Security\Tests\Integration\Services;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Http;
use Modules\Security\Models\IgdbToken;
use Modules\Security\Services\OauthTokenService;
use Tests\TestCase;

class OauthTokenServiceTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @var OauthTokenService  */
    private $oAuthTokenService;

    public function setUp(): void
    {
        parent::setUp();
        $this->oAuthTokenService = new OauthTokenService();
    }

    public function testRefreshIgdbToken()
    {
        $igdbToken = factory(IgdbToken::class)->create();

        $expectedAccessToken = $this->faker->asciify('********************');
        $expectedExpiresIn = 5622652;
        Http::fake([
            'https://id.twitch.tv/oauth2/*' => Http::response(['access_token' => $expectedAccessToken, 'expires_in' => $expectedExpiresIn])
        ]);

        $result = $this->oAuthTokenService->refreshIgdbToken($igdbToken);

        $this->assertSame($expectedAccessToken, $result->access_token);
        $this->assertSame($expectedExpiresIn, $result->expires_in);
    }
}
