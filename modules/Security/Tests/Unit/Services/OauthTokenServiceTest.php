<?php

namespace Modules\Security\Tests\Unit\Services;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Http;
use Mockery;
use Modules\Security\Models\IgdbToken;
use Modules\Security\Services\OauthTokenService;
use Tests\TestCase;

class OauthTokenServiceTest extends TestCase
{
    use WithFaker;

    /** @var Mockery\LegacyMockInterface|Mockery\MockInterface|IgdbToken  */
    private $igdbToken;

    /** @var OauthTokenService  */
    private $oAuthTokenService;

    public function setUp(): void
    {
        parent::setUp();
        $this->igdbToken = Mockery::mock(IgdbToken::class);
        $this->oAuthTokenService = new OauthTokenService();
    }

    public function testRefreshIgdbToken()
    {
        $this->igdbToken
            ->shouldReceive('update')
            ->once();

        Http::fake([
            'https://id.twitch.tv/oauth2/*' => Http::response(['access_token' => $this->faker->asciify('********************'), 'expires_in' => 5622652])
        ]);

        $this->oAuthTokenService->refreshIgdbToken($this->igdbToken);
    }
}
