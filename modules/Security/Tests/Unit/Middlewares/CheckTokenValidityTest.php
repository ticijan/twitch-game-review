<?php

namespace Modules\Security\Tests\Unit\Middlewares;

use Illuminate\Http\Request;
use Mockery;
use Modules\Security\Http\Middleware\CheckTokenValidity;
use Modules\Security\Models\IgdbToken;
use Modules\Security\Repositories\OauthTokenRepository;
use Modules\Security\Services\OauthTokenService;
use Tests\TestCase;

class CheckTokenValidityTest extends TestCase
{
    /** @var Request  */
    private $request;

    private $igdbToken;

    /** @var Mockery\LegacyMockInterface|Mockery\MockInterface|OauthTokenRepository  */
    private $oAuthTokenRepository;

    /** @var Mockery\LegacyMockInterface|Mockery\MockInterface|OauthTokenService  */
    private $oAuthTokenService;

    private $middleware;

    public function setUp(): void
    {
        parent::setUp();
        $this->request = new Request();
        $this->igdbToken = Mockery::mock(IgdbToken::class);
        $this->oAuthTokenRepository = Mockery::mock(OauthTokenRepository::class);
        $this->oAuthTokenService = Mockery::mock(OauthTokenService::class);
        $this->middleware = new CheckTokenValidity($this->oAuthTokenRepository, $this->oAuthTokenService);

        $this->oAuthTokenRepository
            ->shouldReceive('getIgdbToken')
            ->once()
            ->andReturn($this->igdbToken);
    }

    public function testHandleExpiredToken()
    {
        $this->igdbToken
            ->shouldReceive('isExpired')
            ->once()
            ->andReturn(true);

        $this->oAuthTokenService
            ->shouldReceive('refreshIgdbToken')
            ->once();

        $this->middleware->handle($this->request, function (){});
    }

    public function testHandleValidToken()
    {
        $this->igdbToken
            ->shouldReceive('isExpired')
            ->once()
            ->andReturn(false);

        $this->middleware->handle($this->request, function (){});
    }
}
