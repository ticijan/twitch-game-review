<?php

namespace Modules\Security\Http\Middleware;

use Closure;
use Modules\Security\Repositories\OauthTokenRepositoryInterface;
use Modules\Security\Services\OauthTokenServiceInterface;

class CheckTokenValidity
{
    /** @var OauthTokenRepositoryInterface  */
    private $oAuthTokenRepository;

    /** @var OauthTokenServiceInterface  */
    private $oAuthTokenService;

    public function __construct(
        OauthTokenRepositoryInterface $oauthTokenRepository,
        OauthTokenServiceInterface $oAuthTokenService
    ) {
        $this->oAuthTokenRepository = $oauthTokenRepository;
        $this->oAuthTokenService = $oAuthTokenService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->checkIgdbToken();

        return $next($request);
    }

    /** @return bool */
    private function checkIgdbToken()
    {
        $igdbToken = $this->oAuthTokenRepository->getIgdbToken();

        if ($igdbToken->isExpired()) {
            $this->oAuthTokenService->refreshIgdbToken($igdbToken);
        }

        return true;
    }
}
