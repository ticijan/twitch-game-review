<?php

namespace Modules\Security\Providers;

use Illuminate\Support\ServiceProvider;

class DatabaseServiceProvider extends ServiceProvider
{
    const DATABASE_PATH = '/modules/Security/Database';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadDatabaseFiles();
    }

    /**
     * Load Application database files
     */
    protected function loadDatabaseFiles()
    {
        $this->publishes([
            base_path() . self::DATABASE_PATH . '/Migrations' => database_path('migrations')
        ]);

        $this->publishes([
            base_path() . self::DATABASE_PATH . '/Factories' => database_path('factories')
        ]);

        $this->loadMigrationsFrom(base_path() . self::DATABASE_PATH . '/Migrations');
        $this->loadFactoriesFrom(base_path() . self::DATABASE_PATH . '/Factories');
    }
}
