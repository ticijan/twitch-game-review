<?php

namespace Modules\Security\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Security\Repositories\OauthTokenRepository;
use Modules\Security\Repositories\OauthTokenRepositoryInterface;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            OauthTokenRepositoryInterface::class,
            OauthTokenRepository::class
        );
    }
}
