<?php

namespace Modules\Security\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Security\Services\OauthTokenService;
use Modules\Security\Services\OauthTokenServiceInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            OauthTokenServiceInterface::class,
            OauthTokenService::class
        );
    }
}
