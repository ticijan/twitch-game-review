<?php

namespace Modules\Security\Repositories;

use Modules\Security\Models\IgdbToken;

class OauthTokenRepository implements OauthTokenRepositoryInterface
{
    /** @inheritDoc */
    public function getIgdbToken()
    {
        return IgdbToken::first();
    }
}
