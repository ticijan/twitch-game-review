<?php

namespace Modules\Security\Repositories;

use Modules\Security\Models\IgdbToken;

interface OauthTokenRepositoryInterface
{
    /** @return IgdbToken|null */
    public function getIgdbToken();
}
