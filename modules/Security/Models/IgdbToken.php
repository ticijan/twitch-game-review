<?php

namespace Modules\Security\Models;

class IgdbToken extends OauthToken
{
    protected $table = 'oauth_tokens';

    /** @inheritDoc */
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->where('token_for', OauthToken::IGDB_TOKEN);
        });
    }

    /** @inheritDoc */
    public function getTokenType()
    {
        return OauthToken::IGDB_TOKEN;
    }
}
