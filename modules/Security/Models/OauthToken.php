<?php

namespace Modules\Security\Models;

use Illuminate\Database\Eloquent\Model;

abstract class OauthToken extends Model
{
    const IGDB_TOKEN = 'igdb';
    const INTERNAL_TOKEN = 'internal';

    /** @inheritDoc */
    protected $table = 'oauth_tokens';

    /** @inheritDoc */
    protected $fillable = [
        'access_token', 'expires_in', 'refresh_token',
    ];

    /** @return bool */
    public function isExpired()
    {
        return now()->gte($this->updated_at->addSeconds($this->expires_in));
    }

    /** @return string */
    abstract function getTokenType();
}
