<?php

namespace Modules\Reviews\Formatters;

use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Modules\Reviews\ImageSize\IgdbImageSize;

class IgdbSingleGameFormatter implements IgdbSingleGameFormatterInterface
{
    const YOUTUBE_VIDEO_BASE_URL = 'https://youtube.com/embed/';

    /** @inheritDoc */
    public function format(array $game): array
    {
        $game['cover_image_url'] = $this->formatCoverImageUrl($game);

        $game['genres'] = $this->formatGenres($game);

        $game['involved_companies'] = $this->formatInvolvedCompanies($game);

        $game['platforms'] = $this->formatPlatforms($game);

        $game['member_rating'] = $this->formatMemberRating($game);

        $game['critic_rating'] = $this->formatCriticRating($game);

        $game['trailer'] = $this->formatTrailer($game);

        $game['screenshots'] = $this->formatScreenshots($game);

        $game['similar_games'] = $this->formatSimilarGames($game);

        $game['first_release_date'] = $this->formatFirstReleaseDate($game);

        return $game;
    }

    /**
     * @param array $game
     * @return string
     */
    private function formatCoverImageUrl(array $game): string
    {
        return isset($game['cover']['url']) ?
            Str::replaceFirst(
                'thumb',
                IgdbImageSize::BIG,
                $game['cover']['url']
            ) :
            asset('gamePlaceholder.png');
    }

    /**
     * @param array $game
     * @return string|null
     */
    private function formatGenres(array $game)
    {
        return isset($game['genres']) ?
            collect($game['genres'])
                ->pluck('name')
                ->implode(', ') :
            "";
    }

    /**
     * @param array $game
     * @return string|null
     */
    private function formatInvolvedCompanies(array $game)
    {
        return isset($game['involved_companies']) ?
            $game['involved_companies'][0]['company']['name'] :
            '';
    }

    /**
     * @param array $game
     * @return string|null
     */
    private function formatPlatforms(array $game)
    {
        return isset($game['platforms']) ?
            collect($game['platforms'])
                ->pluck('abbreviation')
                ->implode(', ') :
            '';
    }

    /**
     * @param array $game
     * @return string
     */
    private function formatMemberRating(array $game): string
    {
        return array_key_exists('rating', $game) ?
            round($game['rating'], 2) . '%' :
            '0%';
    }

    /**
     * @param array $game
     * @return string
     */
    private function formatCriticRating(array $game): string
    {
        return array_key_exists('aggregated_rating', $game) ?
            round($game['aggregated_rating'], 2) . '%' :
            '0%';
    }

    /**
     * @param array $game
     * @return string|null
     */
    private function formatTrailer(array $game)
    {
        return isset($game['videos']) ?
            self::YOUTUBE_VIDEO_BASE_URL . $game['videos'][0]['video_id'] :
            '';
    }

    /**
     * @param array $game
     * @return \Illuminate\Support\Collection|null
     */
    private function formatScreenshots(array $game)
    {
        return isset($game['screenshots']) ?
            collect($game['screenshots'])
                ->map(function ($screenshot) {
                    return $this->formatSingleScreenshot($screenshot);
                })
                ->take(4) :
            null;
    }

    /**
     * @param array $screenshot
     * @return array
     */
    private function formatSingleScreenshot(array $screenshot): array
    {
        return [
            'big' => Str::replaceFirst(
                'thumb',
                IgdbImageSize::SCREENSHOT_BIG,
                $screenshot['url']
            ),
            'huge' => Str::replaceFirst(
                'thumb',
                IgdbImageSize::SCREENSHOT_HUGE,
                $screenshot['url']
            )
        ];
    }

    /**
     * @param array $game
     * @return \Illuminate\Support\Collection|null
     */
    private function formatSimilarGames(array $game)
    {
        return isset($game['similar_games']) ?
            collect($game['similar_games'])
                ->map(function ($game) {
                    return $this->formatSingleSimilarGame($game);
                })
                ->take(8) :
            null;
    }

    /**
     * @param array $game
     * @return array
     */
    private function formatSingleSimilarGame(array $game): array
    {
        $game['cover_image_url'] = array_key_exists('cover', $game) ?
            Str::replaceFirst(
                'thumb',
                IgdbImageSize::BIG,
                $game['cover']['url']
            ) :
            asset('gamePlaceholder.png');

        $game['rating'] = isset($game['rating']) ?
            round($game['rating'], 2) . "%" :
            '0%';

        $game['platforms'] = array_key_exists('platforms', $game) ?
            collect($game['platforms'])
                ->pluck('abbreviation')
                ->implode(', ') :
            '';

        return $game;
    }

    /**
     * @param array $game
     * @return string
     */
    private function formatFirstReleaseDate(array $game): string
    {
        return isset($game['first_release_date']) ?
            Carbon::createFromTimestamp($game['first_release_date'])->format('m/d/Y') :
            "";
    }
}
