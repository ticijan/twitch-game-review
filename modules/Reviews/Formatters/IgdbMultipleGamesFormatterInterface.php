<?php

namespace Modules\Reviews\Formatters;

interface IgdbMultipleGamesFormatterInterface
{
    /**
     * @param array $games
     * @param string $imageSize
     * @return array
     */
    public function format(array $games, string $imageSize): array;
}
