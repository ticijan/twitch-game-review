<?php

namespace Modules\Reviews\Formatters;

interface IgdbSingleGameFormatterInterface
{
    /**
     * @param array $game
     * @return array
     */
    public function format(array $game): array;
}
