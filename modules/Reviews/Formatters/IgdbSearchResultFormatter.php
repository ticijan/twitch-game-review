<?php

namespace Modules\Reviews\Formatters;

use Illuminate\Support\Str;
use Modules\Reviews\ImageSize\IgdbImageSize;

class IgdbSearchResultFormatter implements IgdbSearchResultFormatterInterface
{
    /** @inheritDoc */
    public function format(array $games): array
    {
        return array_map(function ($game) {
            $game['game']['cover_image_url'] = isset($game['game']['cover']['url']) ?
                Str::replaceFirst(
                    'thumb',
                    IgdbImageSize::SMALL,
                    $game['game']['cover']['url']
                ) :
                asset('gamePlaceholder.png');

            return $game;
        }, $games);
    }
}
