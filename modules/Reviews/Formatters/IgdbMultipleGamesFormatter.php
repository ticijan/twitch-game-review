<?php

namespace Modules\Reviews\Formatters;

use Illuminate\Support\Str;
use Modules\Reviews\ImageSize\IgdbImageSize;

class IgdbMultipleGamesFormatter implements IgdbMultipleGamesFormatterInterface
{
    /** @inheritDoc */
    public function format(array $games, string $imageSize): array
    {
        return array_map(function ($game) use ($imageSize) {
            if ($imageSize !== IgdbImageSize::THUMB) {
                $game['cover_image_url'] = Str::replaceFirst(
                    'thumb',
                    $imageSize,
                    $game['cover']['url']
                );
            }

            $game['rating'] = isset($game['rating']) ?
                round($game['rating'], 2) . "%" :
                null;

            $game['platforms'] = collect($game['platforms'])
                ->pluck('abbreviation')
                ->implode(', ');

            return $game;
        }, $games);
    }
}
