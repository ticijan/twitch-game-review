<?php

namespace Modules\Reviews\Formatters;

interface IgdbSearchResultFormatterInterface
{
    /**
     * @param array $games
     * @return array
     */
    public function format(array $games): array;
}
