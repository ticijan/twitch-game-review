<?php

namespace Modules\Reviews\Tests\Unit\Services;

use Facades\Modules\Reviews\Tests\Unit\Services\DataProviders\GameDataProvider;
use Illuminate\Support\Carbon;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Modules\Reviews\Services\IgdbApiSetupServiceInterface;
use Modules\Reviews\Services\IgdbComingSoonGamesService;
use Tests\TestCase;

class IgdbComingSoonGamesServiceTest extends TestCase
{
    /** @var LegacyMockInterface|MockInterface|IgdbApiSetupServiceInterface  */
    private $igdbApiSetupService;

    /** @var IgdbComingSoonGamesService  */
    private $igdbComingSoonGamesService;

    public function setUp(): void
    {
        parent::setUp();

        $this->igdbApiSetupService = \Mockery::mock(IgdbApiSetupServiceInterface::class);
        $this->igdbComingSoonGamesService = new IgdbComingSoonGamesService($this->igdbApiSetupService);
    }

    public function testGetGames()
    {
        $expectedReturn = GameDataProvider::getDataForMultipleGames();

        $this->igdbApiSetupService
            ->shouldReceive('executeIgdbRequest')
            ->once()
            ->andReturn($expectedReturn);

        $result = $this->igdbComingSoonGamesService->getGames(Carbon::now());

        for ($i = 0; $i < count($result); $i++) {
            $this->assertSame($result[$i], $expectedReturn[$i]);
        }
    }
}
