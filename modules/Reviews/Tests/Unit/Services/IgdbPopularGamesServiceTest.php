<?php

namespace Modules\Reviews\Tests\Unit\Services;

use Illuminate\Support\Carbon;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Modules\Reviews\Services\IgdbApiSetupServiceInterface;
use Modules\Reviews\Services\IgdbPopularGamesService;
use Tests\TestCase;
use Facades\Modules\Reviews\Tests\Unit\Services\DataProviders\GameDataProvider;

class IgdbPopularGamesServiceTest extends TestCase
{
    /** @var LegacyMockInterface|MockInterface|IgdbApiSetupServiceInterface  */
    private $igdbApiSetupService;

    /** @var IgdbPopularGamesService  */
    private $igdbGamesService;

    public function setUp(): void
    {
        parent::setUp();

        $this->igdbApiSetupService = \Mockery::mock(IgdbApiSetupServiceInterface::class);
        $this->igdbGamesService = new IgdbPopularGamesService($this->igdbApiSetupService);
    }

    public function testGetGames()
    {
        $expectedReturn = GameDataProvider::getDataForMultipleGames();

        $this->igdbApiSetupService
            ->shouldReceive('executeIgdbRequest')
            ->once()
            ->andReturn($expectedReturn);

        $result = $this->igdbGamesService->getGames(Carbon::now()->subMonths(3), Carbon::now());

        for ($i = 0; $i < count($result); $i++) {
            $this->assertSame($result[$i], $expectedReturn[$i]);
        }
    }
}
