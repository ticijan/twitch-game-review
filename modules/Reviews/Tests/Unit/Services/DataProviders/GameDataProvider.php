<?php

namespace Modules\Reviews\Tests\Unit\Services\DataProviders;

use Illuminate\Foundation\Testing\WithFaker;

class GameDataProvider
{
    use WithFaker;

    public function __construct()
    {
        $this->setUpFaker();
    }

    /** @return array[] */
    public function getDataForMultipleGames()
    {
        return [
            [
                'id' => $this->faker->randomNumber(),
                'first_release_date' => $this->faker->dateTime,
                'name' => $this->faker->title,
                'rating' => $this->faker->randomFloat(),
                'slug' => $this->faker->slug,
                'total_rating_count' => $this->faker->randomNumber()
            ],
            [
                'id' => $this->faker->randomNumber(),
                'first_release_date' => $this->faker->dateTime,
                'name' => $this->faker->title,
                'rating' => $this->faker->randomFloat(),
                'slug' => $this->faker->slug,
                'total_rating_count' => $this->faker->randomNumber()
            ],
            [
                'id' => $this->faker->randomNumber(),
                'first_release_date' => $this->faker->dateTime,
                'name' => $this->faker->title,
                'rating' => $this->faker->randomFloat(),
                'slug' => $this->faker->slug,
                'total_rating_count' => $this->faker->randomNumber()
            ]
        ];
    }

    /** @return array */
    public function getDataForSingleGame(): array
    {
        return [
            [
                'id' => $this->faker->randomNumber(),
                'first_release_date' => $this->faker->dateTime,
                'name' => $this->faker->title,
                'rating' => $this->faker->randomFloat(),
                'slug' => $this->faker->slug,
                'total_rating_count' => $this->faker->randomNumber()
            ]
        ];
    }

    public function getDataForSearchedGames()
    {
        return [
            [
                'id' => $this->faker->randomNumber(),
                'game' => [
                    'id' => $this->faker->randomNumber(),
                    'cover' => [
                        'id' => $this->faker->randomNumber(),
                        'url' => '//images.igdb.com/igdb/image/upload/t_thumb/co2r3f.jpg'
                    ],
                    'slug' => $this->faker->slug,
                    'cover_image_url' => '//images.igdb.com/igdb/image/upload/t_cover_small/co2r3f.jpg'
                ],
                'name' => $this->faker->title
            ]
        ];
    }
}
