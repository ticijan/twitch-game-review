<?php

namespace Modules\Reviews\Tests\Unit\Services;

use Facades\Modules\Reviews\Tests\Unit\Services\DataProviders\GameDataProvider;
use Illuminate\Support\Carbon;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Modules\Reviews\Services\IgdbApiSetupServiceInterface;
use Modules\Reviews\Services\IgdbSingleGameService;
use Tests\TestCase;

class IgdbSingleGameServiceTest extends TestCase
{
    /** @var LegacyMockInterface|MockInterface|IgdbApiSetupServiceInterface  */
    private $igdbApiSetupService;

    /** @var IgdbSingleGameService  */
    private $igdbGamesService;

    public function setUp(): void
    {
        parent::setUp();

        $this->igdbApiSetupService = \Mockery::mock(IgdbApiSetupServiceInterface::class);
        $this->igdbGamesService = new IgdbSingleGameService($this->igdbApiSetupService);
    }

    public function testGetGames()
    {
        $expectedReturn = GameDataProvider::getDataForSingleGame();

        $this->igdbApiSetupService
            ->shouldReceive('executeIgdbRequest')
            ->once()
            ->andReturn($expectedReturn);

        $result = $this->igdbGamesService->getGame($expectedReturn[0]['slug']);

        $this->assertSame($result, $expectedReturn[0]);
    }
}
