<?php

namespace Modules\Reviews\Tests\Unit\Services;

use Facades\Modules\Reviews\Tests\Unit\Services\DataProviders\GameDataProvider;
use Illuminate\Support\Carbon;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Modules\Reviews\Services\IgdbApiSetupServiceInterface;
use Modules\Reviews\Services\IgdbSearchGamesService;
use Tests\TestCase;

class IgdbSearchGamesServiceTest extends TestCase
{
    /** @var LegacyMockInterface|MockInterface|IgdbApiSetupServiceInterface  */
    private $igdbApiSetupService;

    /** @var IgdbSearchGamesService  */
    private $igdbGamesService;

    public function setUp(): void
    {
        parent::setUp();

        $this->igdbApiSetupService = \Mockery::mock(IgdbApiSetupServiceInterface::class);
        $this->igdbGamesService = new IgdbSearchGamesService($this->igdbApiSetupService);
    }

    public function testGetGames()
    {
        $expectedReturn = GameDataProvider::getDataForSearchedGames();

        $this->igdbApiSetupService
            ->shouldReceive('executeIgdbRequest')
            ->once()
            ->andReturn($expectedReturn);

        $result = $this->igdbGamesService->searchGame('someSearchString');

        for ($i = 0; $i < count($result); $i++) {
            $this->assertSame($result[$i], $expectedReturn[$i]);
        }
    }
}
