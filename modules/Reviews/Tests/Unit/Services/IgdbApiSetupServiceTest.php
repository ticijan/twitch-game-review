<?php

namespace Modules\Reviews\Tests\Unit\Services;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Http;
use Modules\Reviews\Services\IgdbApiSetupService;
use Modules\Security\Models\IgdbToken;
use Modules\Security\Repositories\OauthTokenRepositoryInterface;
use Tests\TestCase;

class IgdbApiSetupServiceTest extends TestCase
{
    use WithFaker;

    /** @var \Mockery\LegacyMockInterface|\Mockery\MockInterface|IgdbToken  */
    private $igdbToken;

    /** @var \Mockery\LegacyMockInterface|\Mockery\MockInterface|OauthTokenRepositoryInterface  */
    private $oAuthTokenRepository;

    /** @var IgdbApiSetupService  */
    private $igdbApiSetup;

    public function setUp(): void
    {
        parent::setUp();

        $this->oAuthTokenRepository = \Mockery::mock(OauthTokenRepositoryInterface::class);
        $this->igdbApiSetup = new IgdbApiSetupService($this->oAuthTokenRepository);
        $this->igdbToken = \Mockery::mock(IgdbToken::class);
    }

    public function testExecuteIgdbRequest()
    {
        $this->igdbToken
            ->shouldReceive('getAttribute')
            ->with('access_token')
            ->once()
            ->andReturn($this->faker->asciify('********************'));

        $this->oAuthTokenRepository
            ->shouldReceive('getIgdbToken')
            ->once()
            ->andReturn($this->igdbToken);

        Http::fake([
            config('services.igdb.api_base_url') => Http::response([
                'success' => true
            ])
        ]);

        $parameters = $this->getApiParameters();

        $result = $this->igdbApiSetup->executeIgdbRequest(
            $parameters['cacheKey'],
            $parameters['requestType'],
            $parameters['body'],
            $parameters['contentType'],
            $parameters['endpoint']
        );

        $this->assertTrue($result['success']);
    }

    /** @return array */
    private function getApiParameters()
    {
        return [
            'cacheKey' => 'test',
            'requestType' => 'POST',
            'body' => 'select fields *; where 1;',
            'contentType' => 'text/plain',
            'endpoint' => config('services.igdb.api_base_url')
        ];
    }
}
