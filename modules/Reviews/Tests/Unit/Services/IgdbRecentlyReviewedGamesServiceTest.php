<?php

namespace Modules\Reviews\Tests\Unit\Services;

use Facades\Modules\Reviews\Tests\Unit\Services\DataProviders\GameDataProvider;
use Illuminate\Support\Carbon;
use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use Modules\Reviews\Services\IgdbApiSetupServiceInterface;
use Modules\Reviews\Services\IgdbRecentlyReviewedGamesService;
use Tests\TestCase;

class IgdbRecentlyReviewedGamesServiceTest extends TestCase
{
    /** @var LegacyMockInterface|MockInterface|IgdbApiSetupServiceInterface  */
    private $igdbApiSetupService;

    /** @var IgdbRecentlyReviewedGamesService  */
    private $igdbGamesService;

    public function setUp(): void
    {
        parent::setUp();

        $this->igdbApiSetupService = \Mockery::mock(IgdbApiSetupServiceInterface::class);
        $this->igdbGamesService = new IgdbRecentlyReviewedGamesService($this->igdbApiSetupService);
    }

    public function testGetGames()
    {
        $expectedReturn = GameDataProvider::getDataForMultipleGames();

        $this->igdbApiSetupService
            ->shouldReceive('executeIgdbRequest')
            ->once()
            ->andReturn($expectedReturn);

        $result = $this->igdbGamesService->getGames(Carbon::now()->subMonths(2), Carbon::now());

        for ($i = 0; $i < count($result); $i++) {
            $this->assertSame($result[$i], $expectedReturn[$i]);
        }
    }
}
