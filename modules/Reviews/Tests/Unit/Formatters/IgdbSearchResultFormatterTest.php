<?php

namespace Modules\Reviews\Tests\Unit\Formatters;

use Modules\Reviews\Formatters\IgdbSearchResultFormatter;
use Tests\TestCase;

class IgdbSearchResultFormatterTest extends TestCase
{
    /** @var IgdbSearchResultFormatter  */
    private $searchResultFormatter;

    public function setUp(): void
    {
        parent::setUp();

        $this->searchResultFormatter = new IgdbSearchResultFormatter();
    }

    public function testFormat()
    {
        $igdbResponse = $this->getIgdbApiResponseArray();
        $expectedResult = $this->getExpectedResult();

        $result = $this->searchResultFormatter->format($igdbResponse);

        $this->assertSame($expectedResult, $result);
    }

    /** @return array[] */
    private function getIgdbApiResponseArray()
    {
        $parameters = $this->getParameters();

        return [
            [
                'id' => $parameters['id'],
                'game' => [
                    'id' => $parameters['game_id'],
                    'cover' => [
                        'id' => $parameters['game_cover_id'],
                        'url' => '//images.igdb.com/igdb/image/upload/t_thumb/co2r3f.jpg'
                    ],
                    'slug' => $parameters['game_slug']
                ],
                'name' => $parameters['game_name']
            ]
        ];
    }

    /** @return array[] */
    private function getExpectedResult()
    {
        $parameters = $this->getParameters();

        return [
            [
                'id' => $parameters['id'],
                'game' => [
                    'id' => $parameters['game_id'],
                    'cover' => [
                        'id' => $parameters['game_cover_id'],
                        'url' => '//images.igdb.com/igdb/image/upload/t_thumb/co2r3f.jpg'
                    ],
                    'slug' => $parameters['game_slug'],
                    'cover_image_url' => '//images.igdb.com/igdb/image/upload/t_cover_small/co2r3f.jpg'
                ],
                'name' => $parameters['game_name']
            ]
        ];
    }

    /** @return array */
    private function getParameters()
    {
        return [
            'id' => 1,
            'game_id' => 1,
            'game_cover_id' => 1,
            'game_slug' => 'trophy',
            'game_name' => 'Trophy'
        ];
    }
}
