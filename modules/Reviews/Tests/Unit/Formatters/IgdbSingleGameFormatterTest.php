<?php

namespace Modules\Reviews\Tests\Unit\Formatters;

use Modules\Reviews\Formatters\IgdbSingleGameFormatter;
use Tests\TestCase;

class IgdbSingleGameFormatterTest extends TestCase
{
    /** @var IgdbSingleGameFormatter  */
    private $igdbSingleGameFormatter;

    public function setUp(): void
    {
        parent::setUp();

        $this->igdbSingleGameFormatter = new IgdbSingleGameFormatter();
    }

    public function testFormat()
    {
        $igdbResponse = $this->getIgdbResponseArray();
        $expectedResult = $this->getExpectedResult();

        $result = $this->igdbSingleGameFormatter->format($igdbResponse);

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @return array[]
     */
    private function getIgdbResponseArray()
    {
        $parameters = $this->getIgdbParameters();

        return [
            'id' => $parameters['id'],
            'cover' => [
                'id' => $parameters['cover_id'],
                'url' => '//images.igdb.com/igdb/image/upload/t_thumb/co2r3f.jpg'
            ],
            'first_release_date' => $parameters['first_release_date'],
            'genres' => [
                [
                    'id' => $parameters['genres_id'],
                    'name' => $parameters['genres_name']
                ]
            ],
            'involved_companies' => [
                [
                    'id' => $parameters['involved_companies_id'],
                    'company' => [
                        'id' => $parameters['involved_companies_company_id'],
                        'name' => $parameters['involved_companies_company_name']
                    ]
                ]
            ],
            'name' => $parameters['name'],
            'rating' => $parameters['rating'],
            'aggregated_rating' => $parameters['aggregated_rating'],
            'platforms' => [
                [
                    'id' => 1,
                    'abbreviation' => 'PC'
                ],
                [
                    'id' => 2,
                    'abbreviation' => 'PS5'
                ]
            ],
            'screenshots' => [
                [
                    'id' => 1,
                    'alpha_channel' => true,
                    'animated' => false,
                    'game' => 1,
                    'height' => 897,
                    'image_id' => 'sc90f1',
                    'url' => '//images.igdb.com/igdb/image/upload/t_thumb/sc90f1.jpg',
                    'width' => 1278,
                    'checksum' => '1062ede8-d578-4152-0d7d-165eecdbd35e'
                ]
            ],
            'similar_games' => [
                [
                    'id' => $parameters['similar_game_id'],
                    'cover' => [
                        'id' => $parameters['similar_game_cover_id'],
                        'url' => '//images.igdb.com/igdb/image/upload/t_thumb/sc90f1.jpg'
                    ],
                    'name' => $parameters['similar_game_name'],
                    'platforms' => [
                        [
                            'id' => 1,
                            'abbreviation' => 'PC'
                        ]
                    ],
                    'rating' => $parameters['similar_game_rating'],
                    'slug' => $parameters['similar_game_slug']
                ]
            ],
            'slug' => $parameters['slug'],
            'summary' => $parameters['summary'],
            'videos' => [
                [
                    'id' => $parameters['video_id'],
                    'game' => $parameters['video_game_id'],
                    'name' => $parameters['video_name'],
                    'video_id' => $parameters['video_video_id'],
                    'checksum' => $parameters['video_checksum']
                ]
            ]
        ];
    }

    /**
     * @return array[]
     */
    private function getExpectedResult()
    {
        $parameters = $this->getIgdbParameters();

        return [
            'id' => $parameters['id'],
            'cover' => [
                'id' => $parameters['cover_id'],
                'url' => '//images.igdb.com/igdb/image/upload/t_thumb/co2r3f.jpg'
            ],
            'first_release_date' => $parameters['first_release_date'],
            'genres' => $parameters['genres_name'],
            'involved_companies' => $parameters['involved_companies_company_name'],
            'name' => $parameters['name'],
            'platforms' => 'PC, PS5',
            'rating' => $parameters['rating'],
            'aggregated_rating' => $parameters['aggregated_rating'],
            'screenshots' => collect([
                [
                    'big' => '//images.igdb.com/igdb/image/upload/t_screenshot_big/sc90f1.jpg',
                    'huge' => '//images.igdb.com/igdb/image/upload/t_screenshot_huge/sc90f1.jpg'
                ]
            ]),
            'similar_games' => collect([
                [
                    'id' => $parameters['similar_game_id'],
                    'cover' => [
                        'id' => $parameters['similar_game_cover_id'],
                        'url' => '//images.igdb.com/igdb/image/upload/t_thumb/sc90f1.jpg'
                    ],
                    'rating' => round($parameters['similar_game_rating'], 2) . "%",
                    'name' => $parameters['similar_game_name'],
                    'platforms' => "PC",
                    'slug' => $parameters['similar_game_slug'],
                    'cover_image_url' => '//images.igdb.com/igdb/image/upload/t_cover_big/sc90f1.jpg'
                ]
            ]),
            'slug' => $parameters['slug'],
            'summary' => $parameters['summary'],
            'videos' => [
                [
                    'id' => $parameters['video_id'],
                    'game' => $parameters['video_game_id'],
                    'name' => $parameters['video_name'],
                    'video_id' => $parameters['video_video_id'],
                    'checksum' => $parameters['video_checksum']
                ]
            ],
            'cover_image_url' => '//images.igdb.com/igdb/image/upload/t_cover_big/co2r3f.jpg',
            'member_rating' => round($parameters['rating'], 2) . '%',
            'critic_rating' => round($parameters['aggregated_rating'], 2) . '%',
            'trailer' => 'https://youtube.com/watch/' . $parameters['video_video_id']
        ];
    }

    /**
     * @return array
     */
    private function getIgdbParameters()
    {
        return [
            'id' => 1,
            'cover_id' => 1,
            'first_release_date' => 1609200000,
            'genres_id' => 1,
            'involved_companies_id' => 1,
            'involved_companies_company_id' => 1,
            'involved_companies_company_name' => 'Platform',
            'genres_name' => 'Platform',
            'involved_companies' => 'Gradual Games',
            'name' => 'Trophy',
            'rating' => 75.2235112,
            'aggregated_rating' => 72.23345555,
            'similar_game_id' => 1,
            'similar_game_name' => 'Super Mario',
            'similar_game_cover_id' => 1,
            'similar_game_rating' => 92.11111111,
            'similar_game_slug' => 'super-mario',
            'slug' => 'trophy',
            'summary' => 'This is an awesome game',
            'video_id' => 1,
            'video_game_id' => 1,
            'video_name' => 'Trailer',
            'video_video_id' => '5_0Vsser',
            'video_checksum' => '40383d34-ab41-c5e4-d755-b6a6bdcb3953'
        ];
    }
}
