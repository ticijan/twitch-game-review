<?php

namespace Modules\Reviews\Tests\Unit\Formatters;

use Illuminate\Foundation\Testing\WithFaker;
use Modules\Reviews\Formatters\IgdbMultipleGamesFormatter;
use Modules\Reviews\ImageSize\IgdbImageSize;
use Tests\TestCase;

class IgdbMultipleGamesFormatterTest extends TestCase
{
    use WithFaker;

    /** @var IgdbMultipleGamesFormatter  */
    private $igdbMultipleGamesFormatter;

    public function setUp(): void
    {
        parent::setUp();

        $this->igdbMultipleGamesFormatter = new IgdbMultipleGamesFormatter();
    }

    public function testFormat()
    {
        $igdbResponse = $this->getIgdbApiResponseArray();
        $expectedResult = $this->getExpectedResult();

        $result = $this->igdbMultipleGamesFormatter->format($igdbResponse, IgdbImageSize::BIG);

        $this->assertSame($expectedResult, $result);
    }

    /** @return array[] */
    private function getIgdbApiResponseArray()
    {
        $parameters = $this->getParameters();

        return [
            [
                'id' => $parameters['id'],
                'cover' => [
                    'id' => $parameters['cover_id'],
                    'url' => '//images.igdb.com/igdb/image/upload/t_thumb/co2r2l.jpg'
                ],
                'first_release_date' => $parameters['first_release_date'],
                'name' => $parameters['name'],
                'platforms' => [
                    [
                        'id' => $this->faker->randomNumber(),
                        'abbreviation' => 'PC'
                    ],
                    [
                        'id' => $this->faker->randomNumber(),
                        'abbreviation' => 'PS5'
                    ]
                ],
                'slug' => $parameters['slug']
            ],
            [
                'id' => $parameters['id'],
                'cover' => [
                    'id' => $parameters['cover_id'],
                    'url' => '//images.igdb.com/igdb/image/upload/t_thumb/co2r2l.jpg'
                ],
                'first_release_date' => $parameters['first_release_date'],
                'rating' => $parameters['rating'],
                'name' => $parameters['name'],
                'platforms' => [
                    [
                        'id' => $this->faker->randomNumber(),
                        'abbreviation' => 'PC'
                    ],
                    [
                        'id' => $this->faker->randomNumber(),
                        'abbreviation' => 'PS5'
                    ]
                ],
                'slug' => $parameters['slug']
            ]
        ];
    }

    /** @return array[] */
    private function getExpectedResult()
    {
        $parameters = $this->getParameters();

        return [
            [
                'id' => $parameters['id'],
                'cover' => [
                    'id' => $parameters['cover_id'],
                    'url' => '//images.igdb.com/igdb/image/upload/t_thumb/co2r2l.jpg'
                ],
                'first_release_date' => $parameters['first_release_date'],
                'name' => $parameters['name'],
                'platforms' => "PC, PS5",
                'slug' => $parameters['slug'],
                'cover_image_url' => '//images.igdb.com/igdb/image/upload/t_cover_big/co2r2l.jpg',
                'rating' => null
            ],
            [
                'id' => $parameters['id'],
                'cover' => [
                    'id' => $parameters['cover_id'],
                    'url' => '//images.igdb.com/igdb/image/upload/t_thumb/co2r2l.jpg'
                ],
                'first_release_date' => $parameters['first_release_date'],
                'rating' => round($parameters['rating'], 2) . "%",
                'name' => $parameters['name'],
                'platforms' => "PC, PS5",
                'slug' => $parameters['slug'],
                'cover_image_url' => '//images.igdb.com/igdb/image/upload/t_cover_big/co2r2l.jpg'
            ]
        ];
    }

    /** @return array */
    private function getParameters()
    {
        return [
            'id' => 1,
            'cover_id' => 1,
            'first_release_date' => 1611705600,
            'rating' => 70.2315552,
            'name' => 'MEGALITH',
            'slug' => 'megalith'
        ];
    }
}
