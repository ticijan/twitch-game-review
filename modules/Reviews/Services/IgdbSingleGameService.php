<?php

namespace Modules\Reviews\Services;

use Illuminate\Support\Str;

class IgdbSingleGameService extends IgdbGamesService implements IgdbSingleGameServiceInterface
{
    /** @inheritDoc */
    public function getGame(string $slug): array
    {
        $parameters = $this->getApiParametersForGames();
        $fields = $this->getIgdbFieldsForSingleGame();

        $body = $this->getBody();
        $body = $this->insertParametersToBody(
            $body,
            $fields,
            $slug
        );

        return $this->igdbApiSetupService
            ->executeIgdbRequest(
                $slug,
                $parameters['requestType'],
                $body,
                $parameters['contentType'],
                $parameters['endpoint']
            )[0];
    }

    /** @inheritDoc */
    public function getBody(): string
    {
        return "
            fields ?
            where slug = \"?\";
        ";
    }

    /**
     * @param string $body
     * @param string $fields
     * @param string $slug
     * @return string
     */
    private function insertParametersToBody(string $body, string $fields, string $slug)
    {
        return Str::replaceArray(
            '?',
            [
                $fields,
                $slug
            ],
            $body
        );
    }
}
