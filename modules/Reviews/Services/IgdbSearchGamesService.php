<?php

namespace Modules\Reviews\Services;

use Illuminate\Support\Str;

class IgdbSearchGamesService extends IgdbGamesService implements IgdbSearchGamesServiceInterface
{
    const SEARCH_GAMES_LIMIT = '2';

    /** @inheritDoc */
    public function searchGame(string $searchString): array
    {
        $parameters = $this->getApiParametersForSearch();
        $fields = $this->getIgdbFieldsForSearchResults();
        $limit = self::SEARCH_GAMES_LIMIT;

        $body = $this->getBody();
        $body = $this->insertParametersToBody(
            $body,
            $fields,
            $searchString,
            $limit
        );

        return $this->igdbApiSetupService
            ->executeIgdbRequest(
                $searchString,
                $parameters['requestType'],
                $body,
                $parameters['contentType'],
                $parameters['endpoint']
            );
    }

    /** @inheritDoc */
    public function getBody(): string
    {
        return "
            fields ?
            search \"?\";
            where game.slug != null;
            where game.cover.url != null;
            limit ?;
        ";
    }

    /**
     * @param string $body
     * @param string $fields
     * @param string $searchString
     * @param string $limit
     * @return string
     */
    private function insertParametersToBody(string $body, string $fields, string $searchString, int $limit)
    {
        return Str::replaceArray(
            '?',
            [
                $fields,
                $searchString,
                $limit
            ],
            $body
        );
    }
}
