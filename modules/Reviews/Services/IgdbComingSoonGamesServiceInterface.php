<?php

namespace Modules\Reviews\Services;

use Illuminate\Support\Carbon;

interface IgdbComingSoonGamesServiceInterface
{
    /**
     * @param Carbon $currentDate
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getGames(Carbon $currentDate, int $limit = 4, int $offset = 0): array;
}
