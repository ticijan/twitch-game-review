<?php

namespace Modules\Reviews\Services;

interface IgdbApiSetupServiceInterface
{
    /**
     * @param string $cacheKey
     * @param string $requestType
     * @param string $body
     * @param string $contentType
     * @param string $endpoint
     * @return array
     */
    public function executeIgdbRequest(string $cacheKey, string $requestType, string $body, string $contentType, string $endpoint): array;
}
