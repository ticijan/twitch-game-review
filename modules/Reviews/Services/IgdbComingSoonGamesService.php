<?php

namespace Modules\Reviews\Services;

use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class IgdbComingSoonGamesService extends IgdbGamesService implements IgdbComingSoonGamesServiceInterface
{
    /** @inheritDoc */
    public function getGames(Carbon $currentDate, int $limit = 4, int $offset = 0): array
    {
        $parameters = $this->getApiParametersForGames();
        $fields = $this->getIgdbFieldsForMultipleGames();
        $platforms = $this->getIgdbPopularPlatforms();

        $body = $this->getBody();
        $body = $this->insertParametersToBody(
            $body,
            $fields,
            $platforms,
            $currentDate->timestamp,
            $limit,
            $offset
        );

        return $this->igdbApiSetupService
            ->executeIgdbRequest(
                'comingSoonGames',
                $parameters['requestType'],
                $body,
                $parameters['contentType'],
                $parameters['endpoint']
            );
    }

    /** @inheritDoc */
    public function getBody(): string
    {
        return "
            fields ?
            where platforms = ? &
            (first_release_date > ?) &
            cover.url != null;
            sort rating_count desc;
            limit ?;
        ";
    }

    /**
     * @param string $body
     * @param string $fields
     * @param string $platforms
     * @param int $timestamp
     * @param int $limit
     * @param int $offset
     * @return string
     */
    private function insertParametersToBody(string $body, string $fields, string $platforms, int $timestamp, int $limit, int $offset)
    {
        return Str::replaceArray(
            '?',
            [
                $fields,
                $platforms,
                $timestamp,
                $limit,
                $offset
            ],
            $body
        );
    }
}
