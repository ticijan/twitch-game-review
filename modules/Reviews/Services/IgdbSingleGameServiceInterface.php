<?php

namespace Modules\Reviews\Services;

interface IgdbSingleGameServiceInterface
{
    /**
     * @param string $slug
     * @return array
     */
    public function getGame(string $slug): array;
}
