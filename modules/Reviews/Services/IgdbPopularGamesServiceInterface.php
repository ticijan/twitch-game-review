<?php

namespace Modules\Reviews\Services;

use Illuminate\Support\Carbon;

interface IgdbPopularGamesServiceInterface
{
    /**
     * @param Carbon $firstReleaseBefore
     * @param Carbon $firstReleaseAfter
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function getGames(Carbon $firstReleaseBefore, Carbon $firstReleaseAfter, int $limit = 8, int $offset = 0): array;
}
