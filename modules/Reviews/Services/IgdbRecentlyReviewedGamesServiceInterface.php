<?php

namespace Modules\Reviews\Services;

use Illuminate\Support\Carbon;

interface IgdbRecentlyReviewedGamesServiceInterface
{
    /**
     * @param Carbon $firstReleaseBefore
     * @param Carbon $firstReleaseAfter
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getGames(Carbon $firstReleaseBefore, Carbon $firstReleaseAfter, int $limit = 8, int $offset = 0): array;
}
