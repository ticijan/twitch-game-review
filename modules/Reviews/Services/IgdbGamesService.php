<?php

namespace Modules\Reviews\Services;

use Illuminate\Support\Carbon;

abstract class IgdbGamesService
{
    /** @var IgdbApiSetupServiceInterface  */
    protected $igdbApiSetupService;

    public function __construct(IgdbApiSetupServiceInterface $igdbApiSetupService)
    {
        $this->igdbApiSetupService = $igdbApiSetupService;
    }

    /** @return string */
    abstract function getBody(): string;

    /** @return string */
    protected function getIgdbFieldsForMultipleGames()
    {
        return "
            name,
            cover.url,
            first_release_date,
            total_rating_count,
            platforms.abbreviation,
            rating,
            summary,
            slug;
        ";
    }

    /** @return string */
    protected function getIgdbFieldsForSingleGame()
    {
        return "
            name,
            cover.url,
            first_release_date,
            platforms.abbreviation,
            total_rating_count,
            rating,
            slug,
            involved_companies.company.name,
            genres.name,
            aggregated_rating,
            summary,
            videos.*,
            screenshots.*,
            similar_games.cover.url,
            similar_games.name,
            similar_games.rating,
            similar_games.platforms.abbreviation,
            similar_games.slug;
        ";
    }

    /** @return string */
    protected function getIgdbFieldsForSearchResults()
    {
        return "
            name,
            game.slug,
            game.cover.url;
        ";
    }

    /** @return string */
    protected function getIgdbPopularPlatforms()
    {
        return "(6, 48, 49, 136)";
    }

    /** @return string[] */
    protected function getApiParametersForGames()
    {
        return [
            "requestType" => "POST",
            "contentType" => "text/plain",
            "endpoint" => config('services.igdb.api_base_url') . "games"
        ];
    }

    /** @return string[] */
    protected function getApiParametersForSearch()
    {
        return [
            "requestType" => "POST",
            "contentType" => "text/plain",
            "endpoint" => config('services.igdb.api_base_url') . "search"
        ];
    }
}
