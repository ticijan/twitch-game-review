<?php

namespace Modules\Reviews\Services;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Modules\Security\Repositories\OauthTokenRepositoryInterface;

class IgdbApiSetupService implements IgdbApiSetupServiceInterface
{
    const REQUEST_TYPE_GET = 'GET';
    const REQUEST_TYPE_POST = 'POST';

    /** @var OauthTokenRepositoryInterface  */
    private $oAuthTokenRepository;

    public function __construct(OauthTokenRepositoryInterface $oauthTokenRepository)
    {
        $this->oAuthTokenRepository = $oauthTokenRepository;
    }

    /**
     * @inheritDoc
     */
    public function executeIgdbRequest(string $cacheKey, string $requestType, string $body, string $contentType, string $endpoint): array
    {
        $accessToken = $this->getIgdbAccessToken();

        $request = Http::withHeaders($this->getIgdbHeaders($accessToken))
            ->withBody($body, $contentType);

        return Cache::remember(
            $cacheKey,
            15,
            function () use ($request, $requestType, $endpoint) {
                $response = $this->sendRequest($request, $requestType, $endpoint);

                return $response->json();
            }
        );
    }

    /** @return string */
    private function getIgdbAccessToken()
    {
        $igdbToken = $this->oAuthTokenRepository->getIgdbToken();

        return $igdbToken->access_token;
    }

    /**
     * @param string $accessToken
     * @return array
     */
    private function getIgdbHeaders(string $accessToken): array
    {
        return [
            'Client-ID' => config('services.igdb.client_id'),
            'Authorization' => 'Bearer ' . $accessToken
        ];
    }

    /**
     * @param PendingRequest $request
     * @param string $requestType
     * @param string $endpoint
     * @return Response
     * @throws \Exception
     */
    private function sendRequest(PendingRequest $request, string $requestType, string $endpoint): Response
    {
        switch ($requestType) {
            case self::REQUEST_TYPE_GET:
                return $request->get($endpoint);
            case self::REQUEST_TYPE_POST:
                return $request->post($endpoint);
            default:
                throw new \Exception("Request type: '{$requestType}' is not supported!");
        }
    }
}
