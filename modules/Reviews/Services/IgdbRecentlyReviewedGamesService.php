<?php

namespace Modules\Reviews\Services;

use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class IgdbRecentlyReviewedGamesService extends IgdbGamesService implements IgdbRecentlyReviewedGamesServiceInterface
{
    /** @inheritDoc */
    public function getGames(Carbon $firstReleaseBefore, Carbon $firstReleaseAfter, int $limit = 8, int $offset = 0): array
    {
        $parameters = $this->getApiParametersForGames();
        $fields = $this->getIgdbFieldsForMultipleGames();
        $platforms = $this->getIgdbPopularPlatforms();

        $body = $this->getBody();
        $body = $this->insertParametersToBody(
            $body,
            $fields,
            $platforms,
            $firstReleaseBefore->timestamp,
            $firstReleaseAfter->timestamp,
            $limit,
            $offset
        );

        return $this->igdbApiSetupService
            ->executeIgdbRequest(
                'recentlyReviewedGames',
                $parameters['requestType'],
                $body,
                $parameters['contentType'],
                $parameters['endpoint']
            );
    }

    /** @inheritDoc */
    public function getBody(): string
    {
        return "
            fields ?
            where platforms = ? &
            (first_release_date > ? &
            first_release_date < ? &
            rating_count > 5) &
            cover.url != null;
            sort rating_count desc;
            limit ?;
            offset ?;
        ";
    }

    /**
     * @param string $body
     * @param string $fields
     * @param string $platforms
     * @param int $firstReleaseDateBefore
     * @param int $firstReleaseDateAfter
     * @param int $limit
     * @param int $offset
     * @return string
     */
    private function insertParametersToBody(string $body, string $fields, string $platforms, int $firstReleaseDateBefore, int $firstReleaseDateAfter, int $limit, int $offset)
    {
        return Str::replaceArray(
            '?',
            [
                $fields,
                $platforms,
                $firstReleaseDateBefore,
                $firstReleaseDateAfter,
                $limit,
                $offset
            ],
            $body
        );
    }
}
