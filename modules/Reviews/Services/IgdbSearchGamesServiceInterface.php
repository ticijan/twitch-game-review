<?php

namespace Modules\Reviews\Services;

interface IgdbSearchGamesServiceInterface
{
    /**
     * @param string $searchString
     * @return array
     */
    public function searchGame(string $searchString): array;
}
