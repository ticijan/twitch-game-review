<?php

namespace Modules\Reviews\ImageSize;

class IgdbImageSize
{
    const BIG = 'cover_big';
    const SMALL = 'cover_small';
    const THUMB = 'thumb';
    const SCREENSHOT_HUGE = 'screenshot_huge';
    const SCREENSHOT_BIG = 'screenshot_big';
}
