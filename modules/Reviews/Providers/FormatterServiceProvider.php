<?php

namespace Modules\Reviews\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Reviews\Formatters\IgdbMultipleGamesFormatter;
use Modules\Reviews\Formatters\IgdbMultipleGamesFormatterInterface;
use Modules\Reviews\Formatters\IgdbSearchResultFormatter;
use Modules\Reviews\Formatters\IgdbSearchResultFormatterInterface;
use Modules\Reviews\Formatters\IgdbSingleGameFormatter;
use Modules\Reviews\Formatters\IgdbSingleGameFormatterInterface;

class FormatterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            IgdbMultipleGamesFormatterInterface::class,
            IgdbMultipleGamesFormatter::class
        );

        $this->app->bind(
            IgdbSingleGameFormatterInterface::class,
            IgdbSingleGameFormatter::class
        );

        $this->app->bind(
            IgdbSearchResultFormatterInterface::class,
            IgdbSearchResultFormatter::class
        );
    }
}
