<?php

namespace Modules\Reviews\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Reviews\Services\IgdbComingSoonGamesService;
use Modules\Reviews\Services\IgdbComingSoonGamesServiceInterface;
use Modules\Reviews\Services\IgdbApiSetupService;
use Modules\Reviews\Services\IgdbApiSetupServiceInterface;
use Modules\Reviews\Services\IgdbPopularGamesService;
use Modules\Reviews\Services\IgdbPopularGamesServiceInterface;
use Modules\Reviews\Services\IgdbRecentlyReviewedGamesService;
use Modules\Reviews\Services\IgdbRecentlyReviewedGamesServiceInterface;
use Modules\Reviews\Services\IgdbSearchGamesService;
use Modules\Reviews\Services\IgdbSearchGamesServiceInterface;
use Modules\Reviews\Services\IgdbSingleGameService;
use Modules\Reviews\Services\IgdbSingleGameServiceInterface;

class ReviewsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            IgdbApiSetupServiceInterface::class,
            IgdbApiSetupService::class
        );

        $this->app->bind(
            IgdbComingSoonGamesServiceInterface::class,
            IgdbComingSoonGamesService::class
        );

        $this->app->bind(
            IgdbPopularGamesServiceInterface::class,
            IgdbPopularGamesService::class
        );

        $this->app->bind(
            IgdbRecentlyReviewedGamesServiceInterface::class,
            IgdbRecentlyReviewedGamesService::class
        );

        $this->app->bind(
            IgdbSingleGameServiceInterface::class,
            IgdbSingleGameService::class
        );

        $this->app->bind(
            IgdbSearchGamesServiceInterface::class,
            IgdbSearchGamesService::class
        );
    }
}
