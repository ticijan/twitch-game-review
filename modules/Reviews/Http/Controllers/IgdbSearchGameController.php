<?php

namespace Modules\Reviews\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use Modules\Reviews\Formatters\IgdbSearchResultFormatterInterface;
use Modules\Reviews\Services\IgdbSearchGamesServiceInterface;

class IgdbSearchGameController
{
    /** @var IgdbSearchGamesServiceInterface  */
    private $searchService;

    /** @var IgdbSearchGamesServiceInterface  */
    private $gamesFormatter;

    public function __construct(
        IgdbSearchGamesServiceInterface $searchService,
        IgdbSearchResultFormatterInterface $gamesFormatter
    ) {
        $this->searchService= $searchService;
        $this->gamesFormatter = $gamesFormatter;
    }

    /**
     * @param string $searchString
     * @return JsonResponse
     */
    public function index(string $searchString)
    {
        try {
            $result = $this->searchService->searchGame($searchString);

            return Response::json([
                'success' => true,
                'games' => $this->gamesFormatter->format($result)
            ], 200);
        } catch (\Exception $exception) {
            return Response::json([
                'success' => false,
                'games' => 'We are sorry but something happened. Please try again later.'
            ], 500);
        }
    }
}
