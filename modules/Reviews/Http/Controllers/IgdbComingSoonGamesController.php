<?php

namespace Modules\Reviews\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Response;
use Modules\Reviews\Formatters\IgdbMultipleGamesFormatterInterface;
use Modules\Reviews\Http\Requests\GamesRequest;
use Modules\Reviews\ImageSize\IgdbImageSize;
use Modules\Reviews\Services\IgdbComingSoonGamesServiceInterface;

class IgdbComingSoonGamesController extends Controller
{
    /** @var IgdbComingSoonGamesServiceInterface  */
    private $gamesService;

    /** @var IgdbMultipleGamesFormatterInterface  */
    private $gamesFormatter;

    public function __construct(
        IgdbComingSoonGamesServiceInterface $gamesService,
        IgdbMultipleGamesFormatterInterface $gamesFormatter
    ) {
        $this->gamesService = $gamesService;
        $this->gamesFormatter = $gamesFormatter;
    }

    /**
     * Returns a listing of games that are coming soon.
     *
     * @param GamesRequest $request
     * @return JsonResponse
     */
    public function index(GamesRequest $request)
    {
        try {
            $games = $this->gamesService
                ->getGames(
                    Carbon::now(),
                    $request->get('limit', 4),
                    $request->get('offset', 0)
                );

            return Response::json([
                'success' => true,
                'games' => $this->gamesFormatter->format($games, IgdbImageSize::SMALL)
            ], 200);
        } catch (\Exception $exception) {
            return Response::json([
                'success' => false,
                'message' => 'We are sorry but something happened. Please try again later.'
            ], 500);
        }
    }
}
