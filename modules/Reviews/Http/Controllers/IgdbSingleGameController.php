<?php

namespace Modules\Reviews\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use Modules\Reviews\Formatters\IgdbSingleGameFormatterInterface;
use Modules\Reviews\Services\IgdbSingleGameServiceInterface;

class IgdbSingleGameController
{
    /** @var IgdbSingleGameServiceInterface  */
    private $gameService;

    /** @var IgdbSingleGameFormatterInterface  */
    private $gameFormatter;

    public function __construct(
        IgdbSingleGameServiceInterface $gameService,
        IgdbSingleGameFormatterInterface $gameFormatter
    ) {
        $this->gameService = $gameService;
        $this->gameFormatter = $gameFormatter;
    }

    /**
     * Returns data for a single game.
     *
     * @param string $slug
     * @return JsonResponse
     */
    public function index(string $slug)
    {
        try {
            $game = $this->gameService->getGame($slug);

            return Response::json([
                'success' => true,
                'games' => $this->gameFormatter->format($game)
            ], 200);
        } catch (\Exception $exception) {
            return Response::json([
                'success' => false,
                'message' => 'We are sorry but something happened. Please try again later.'
            ], 500);
        }
    }
}
