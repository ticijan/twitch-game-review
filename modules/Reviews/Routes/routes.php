<?php

use Illuminate\Support\Facades\Route;

// All route files should have a prefix ( preferred prefix would be the module name ).
// We exclude the FrontEnd module from this rule as it contains the main routes to our application.
Route::prefix('reviews')->group(function () {
    Route::prefix('games')->group(function () {
        Route::get('/soon', 'IgdbComingSoonGamesController@index');
        Route::get('/popular', 'IgdbPopularGamesController@index');
        Route::get('/recent', 'IgdbRecentlyReviewedGamesController@index');
    });

    Route::get('/search/{searchString}', 'IgdbSearchGameController@index');
    Route::get('/game/{slug}', 'IgdbSingleGameController@index');
});
