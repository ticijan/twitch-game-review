<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Modules\Security\Models\IgdbToken;
use Modules\Security\Models\OauthToken;
use Faker\Generator as Faker;

$factory->define(IgdbToken::class, function (Faker $faker) {
    return [
        'access_token' => $faker->asciify('********************'),
        'expires_in' => 5587808,
        'refresh_token' => null,
        'token_for' => OauthToken::IGDB_TOKEN
    ];
});
