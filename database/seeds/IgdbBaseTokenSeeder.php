<?php

use Illuminate\Database\Seeder;
use Modules\Security\Models\IgdbToken;

class IgdbBaseTokenSeeder extends Seeder
{
    /**
     * Run the database seeds to create an IGDB base token.
     *
     * @return void
     */
    public function run()
    {
        $igdbToken = factory(IgdbToken::class)->create();
        $igdbToken->expires_in = 0;
        $igdbToken->save();
    }
}
